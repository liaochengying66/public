#/usr/bin/env python
#_*_coding:utf-8_*_
import time
import requests
import re
import copy


url = 'https://m.new500.cn/kj/cqssc-20190802.htm'

def get_current_date():
    t = time.strftime("%Y%m%d", time.localtime()) 
    return t

def get_last_date(date):
    year = int(date[0:4])
    month = int(date[4:6])
    day = int(date[6:8])
    if day > 1:
        day = day - 1
    else:
        if month > 1:
            month = month - 1
            if month== 4 or month==6 or month==9 or month==11:
                day = 30
            elif month == 2:
                if year % 400 == 0 or year%4==0 and year%100!=0 :
                    day = 29
                else:
                    day = 28
            else:
                day = 31
        else:
            year = year - 1
            month = 12
            day = 31
    return "{0}{1}{2}{3}{4}".format(year,int(month/10),month%10,int(day/10),int(day%10))

def get_next_date(date):                                                                        #输入当前日期，获取下一个日期，日期格式YYYY-MM-DD
    year = int(date[0:4])
    month  = int(date[4:6])
    day = int(date[6:8])
    if month == 2:
        if year%400==0 or year%4==0 and year%100!= 0:
            maxmonth = 29
        else :
            maxmonth = 28
    elif month == 4 or month == 6 or month == 9 or month == 11:
        maxmonth = 30
    else:
        maxmonth = 31
    if day < maxmonth:                                                                    #如果日期小于最大天数，天数加一
        day = day + 1
    else:
        day = 1                                                                           #否则日期变成1号
        if month < 12:                                                                    #如果月份小于12，则月份加一
            month = month + 1
        else:
            month = 1                                                                     #否则月变成1，年加一
            year = year + 1
    return "{0}{1}{2}{3}{4}".format(year,int(month/10),month%10,int(day/10),int(day%10))

def get_history(date):
    history = []
    url = "https://m.new500.cn/kj/cqssc-{0}.htm".format(date)
    print(url)
    response = requests.get(url)
    text = response.text
#    print(text)
    s = re.findall(r'(\d{11}).*?(期).*?(\d{4}-\d{2}-\d{2}.*?\d{2}:\d{2}).*?(\d).*?(\d).*?(\d).*?(\d).*?(\d)',text,flags=re.S)   ##flags增加对换行的匹配
    for i in s :
        history.append(["{0}{1}".format(i[0],i[1]),i[2],"{0}{1}{2}{3}{4}".format(i[3],i[4],i[5],i[6],i[7])])
    return history


def insert(task,count):
    for i in range(5):
        for j in range(10):
            count[i][j][1] += 1
    for i in range(5):
        for j in range(10):
            if count[i][j][0] == int(task[i]):
                count[i][j][1] = 0
                s = copy.deepcopy(count[i][j])
                for k in range(j):
                    count[i][j-k] = copy.deepcopy(count[i][j-k-1])
                count[i][0] = s
                break
    return count




if __name__ == '__main__':
    count = 2000
    t = 0                                             #每天的期数
    t2 = 0                                            #历史天数
    date =get_current_date()
    date = get_last_date(date)
    t = len(get_history(date))
    while True:
        length = len(get_history(date))
        count = count - length
        if t != length or count < 0:
            break
        date = get_last_date(date)
        t2 = t2 + 1
    begin = "{0}年{1}月{2}日".format(date[0:4],date[4:6],date[6:8])
    count = []
    for i in range(5):
        count.append([])
        for j in range(10):
            count[i].append([j,0])
#    print(count)
    for i in range(t2+1):
        history = get_history(date)
        for task in history[::-1]:
            count = insert(task[2],count)
        date = get_next_date(date)
    history = get_history(date)
    for task in history[::-1]:
        print(task[2])
        count = insert(task[2],count)
    print(begin)
    for i in range(10):
        print("{0}\t{1}\t{2}\t{3}\t{4}".format(count[0][i],count[1][i],count[2][i],count[3][i],count[4][i]))
#    while True:
#        input = 
