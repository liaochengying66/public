#!/user/bin/python
#__*__ coding:utf-8__*_
import requests
import json
import traceback
import os
import stat

def get():
    response = requests.get("https://kaijiang.aicai.com/cqssc/")                            #获取当前结果
    text = response.text                                                                    #获取页面内容
#    text = text.encode('unicode-escape')
    text = text.split('<tbody id="jq_body_kc_result">')
    text = text[1].split('<div class="lotboxright mt10">')                                  #筛选出需要的内容
    text = text[0].split('<td>')
    i=1
    data = {}
    while i <len(text):
        y = text[i].split('</td>')                                                           #获取期号y[0]
        x = y[1].split('<td class="tdright">')                                               #获取时间x[1]
        z = y[2].split('<td style="color:red">')
        z = z[1].split('|')
        z = str(z[0])+str(z[1])+str(z[2])+str(z[3])+str(z[4])                                #获取开奖号码
        print(z)
        s = y[0]+x[1]
        data[s] = z
        i = i + 1 
    return data

#print(get())

def post(date,log):                                                                         #传入日期和存档文件
    data  = {"gameIndex":301,"searchDate":date}                                             #定义token
    response = requests.post("https://kaijiang.aicai.com/open/kcResultByDate.do",data=data) #请求数据
    text = response.text                                                                    #获取请求内容
#    text = text.encode('unicode-escape')
    text = text.split('<td>')
    if len(text) <= 120:
        return 0
    i = len(text) - 1 
    x = 0
    y = 0
    Data = []
    myfile = open(log,'a')
    while i > 0:
        try:
            y = text[i].split('"')
        except:
            return 0
        y = y[0].split('</td>')                                                           #获取期号y[0]
        x = y[1].split('<td class=\'tdright\'>')                                          #获取时间x[1]
        z = y[2].split('<td style=\'color:red\'>')
        z = z[1].split("|")
        z = str(z[0]) +  str(z[1]) +  str(z[2]) +  str(z[3]) +  str(z[4])                 #获取开奖号
        S = y[0]+':'+ z  +':' + '(' + x[1]  + ')' +'\n'
        myfile.write(S)                                                                    #写入文档保存
        i = i - 1
    myfile.close()
    return len(text) - 1                                                                   #返回期数


#post("2010-02-01",'a.txt')

def get_date(date):                                                                        #输入当前日期，获取下一个日期，日期格式YYYY-MM-DD
    date = date.split('-')
    year = int(date[0])
    day  = int(date[2])
    month = int(date[1])
    maxmonth = 0                                                                           #记录当前月的最大天数
    if month == 2:
        if year % 400 == 0 or year % 4 == 0 and year % 100 != 0:
            maxmonth = 29
        else :
            maxmonth = 28
    elif month == 4 or month == 6 or month == 9 or month == 11:
        maxmonth = 30
    else:
        maxmonth = 31
    if day < maxmonth:                                                                    #如果日期小于最大天数，天数加一
        day = day + 1
    else:
        day = 1                                                                           #否则日期变成1号
        if month < 12:                                                                    #如果月份小于12，则月份加一
            month = month + 1
        else:
            month = 1                                                                     #否则月变成1，年加一
            year = year + 1
    if month < 10:
        month = "0"+str(month)
    else:
        month = str(month)
    if day < 10:
        day = "0"+str(day)
    else:
        day = str(day)
    date = str(year) +'-'+ month+'-'+day
    return date

def check(date1,date2):
    date =date1
    logname = date
    file=open(logname,"a")
    file.close()
    while date != date2:
        count = post(date,logname)
        if count != 120:
            if date == logname:
                date = get_date(date)
                os.rename(logname,date)
                logname = date
            else:
                logname2 = logname+'--'+date
                os.rename(logname,logname2)
                date = get_date(date)
                logname = date
                file=open(logname,"a")
                file.close()
        else:
           date = get_date(date) 
        print(date+":",end=''),
        print(count) 




if __name__ == '__main__':
#    print get()
#    post()
    check("2009-01-01","2019-05-08")
