#!/user/bin/python
#_*_coding:utf-8_*_
import sys   


def  read(file):                                 #打开文件，并返回期号
    f=open(file)
    k=f.read()
    s = []
    k = k.split('\n')
    for i in range(len(k)-1):
        t = k[i].split(':')
        s.append(t[1])
    return s

def sort(count,k):                               #将新的数插入到未出现次数排序中，并返回插入之前数的序号
    t = 0
    for i in range(100):
        count[i][1] = count[i][1] + 1
    for i in range(100):
        if count[i][0] == k:
            t = i
            count[i][1] = 0
            break
    now = count[t]
    for i in range(t):
        count[t-i] = count[t-i-1]
    count[0] = now
    return count,t


def check99(count,x):                         #统计未出现次数排序的出现次数以及当前未出现次数和最大未出现次数
    for i in range(100):
        if i != x:
            count[i][1] = count[i][1] + 1
            if count[i][1] > count[i][2]:
                count[i][2] = count[i][1]
        else:
            count[i][0] = count[i][0] + 1
            count[i][1] = 0
    return count


def check10(count,x):
    for i in range(10):
        if i != int(x/10):
            count[i][1] = count[i][1] +1
            if count[i][1] > count[i][2]:
                count[i][2] = count[i][1]
        else:
            count[i][0] = count[i][0] + 1
            count[i][1] = 0
    return count


def count(file,log=None):
    count = [0]*100                         #存放每个号出现的次数
    count2 = []                             #统计每个号的未出现次数并排序
    count3 = []                             #统计未出现次数排序的未出现次数和未出现次数以及最大未出现次数
    count4 = []                             #以10为一个维度统计
    for i in range(10):
        count4.append([0,0,0])
    x = 0
    max = 0
    n = 0                                  #统计总期数
    n3 = 0                                 #统计排序总期数
    n4 = 0                                 #统计10个为一维度总期数
    for i in range(100):
        count2.append([i,0])                #初始化每个数未出现次数为0
        count3.append([0,0,0])              #初始化出现次数为零，当前未出现次数为0，最大未出现次数为0
#    print(count2)
    s = read(file)
    for i in range(len(s)):
        k = s[i][3]+s[i][4]
        t = int(k)
        count[t] = count[t] + 1
        n = n + 1
        count2,x = sort(count2,t)
        if max < count2[99][1]:
            max = count2[99][1]
        if count2[89][1]  != count2[90][1]:
            count4 = check10(count4,x)
            n4 = n4 + 1
        if count2[98][1] != count2[99][1]:
            count3 = check99(count3,x)
            n3 = n3 + 1
    f = open(log,'a')
    f.write(file+'\n')
    f.write('每个号出现的次数：'+'\n')
    for i in range(100):
        rate = (float(count[i])/float(n)) * 100
        rate = round(rate,2)
        f.write(str(i)+':'+str(count[i])+':'+str(rate)+'\t')
        if (i+1) % 5 == 0:
            f.write('\n')
    if n3 != 0:
        f.write('\n'+'各个位未出现次数出现排序后出现次数以及最大未出现次数：'+'\n')
        f.write('排序   出现次数    最大未现'+'\n')
        for i in range(100):
            rate = (float(count3[i][0])/float(n3)) * 100
            rate = round(rate,2)
            f.write(str(i)+'\t'+str(count3[i][0])+'\t' + str(count3[i][2]) + '\t'+str(rate)+'\n')
        f.write('\n'+'每十位未出现次数出现排序后出现次数以及最大未出现次数：'+'\n')
        for i in range(10):
            rate = (float(count4[i][0])/float(n4)) * 100
            rate = round(rate,2)
            f.write(str(i*10)+'到'+ str(i*10+9) +  '\t'+str(count4[i][0])+'\t' + str(count4[i][2])+'\t'+str(rate) + '\n')
    f.write('\n'+'最多次未出现的次数：' + str(max)+'\n')
    f.write('\n'+'\n'+'\n'+'\n'+'\n')
    f.close()


def get_name():                             #获取文件目录，返回文件目录列表
    f=open('data.txt')
    k=f.read()
    k = k.split('\n')
    return k

if __name__ == '__main__':
    name = get_name()
    for i in name:
        count(i,'tt.txt')
